# govtech

## overview

backend is set up using nodejs, typescript, express, and mongodb

nodejs was chosen due to the need for quick, lightweight iterations and development
mongodb was chosen due to the simplicity in hosting and scalability (IPAM systems can be very large. noSQL handles this well through sharding)

## schema overview

two models were used in the IPAM solution, cidr_range and cidr_block. the models have redundant data to aid in data retrieval and manipulation.

## best practices adhered to

- all db values will be written in snake_case
- all endpoints will be written in snake_case too (the given examples use kebab case instead. i chose not to follow this as it would add inconsistency into the backend )
- zod was used to enforce data integrity

## additional features implemented

- deployed backend to vercel
- hosted db using mongodb
- implemented a CI pipeline (was unable to integrate tests into it due to errors with jest and async)
- created a testing suite to test the basic functions of the server
- created addtional endpoints to aid in testing

## how to start

1. yarn
2. yarn dev to run locally on port 3000

to access the version hosted on vercel, please go to [https://govtech-tap.vercel.app/](https://govtech-tap.vercel.app/)

this repo contains a postman.json of the endpoints for your reference
