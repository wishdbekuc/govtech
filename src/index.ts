import { PrismaClient } from '@prisma/client';
import express from 'express';
import routes from './routes';
import cors from 'cors';

export const prisma = new PrismaClient({
  log: process.env.NODE_ENV === 'dev' ? ['warn', 'error'] : [],
});
const app = express();
app.use(cors());

app.use(express.json());

app.use('', routes);

app.listen(3000, () => console.log('🚀 Govtech server ready at port 3000'));

export default app;
