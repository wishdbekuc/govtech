import express from 'express';
const router = express.Router();
import CIDRRangeRoutes from './CIDR_range';

router.use('/cidr-ranges', CIDRRangeRoutes);

export default router;
