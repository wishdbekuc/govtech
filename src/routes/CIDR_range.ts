import express from 'express';
const CIDRRangeRoutes = express.Router();
import CIDRRangeController from '../controllers/CIDR_range';

CIDRRangeRoutes.get('/', CIDRRangeController.getAllCIDRRanges); // get all CIDR ranges
CIDRRangeRoutes.get('/:CIDR_range_id/blocks', CIDRRangeController.getAllCIDRBlocks); // get all CIDR ranges
CIDRRangeRoutes.post('/', CIDRRangeController.createCIDRRange); // create CIDR range
CIDRRangeRoutes.post('/:CIDR_range_id/blocks', CIDRRangeController.createCIDRBlock); // create CIDR block
CIDRRangeRoutes.delete('/:CIDR_range_id/blocks/:CIDR_block_id', CIDRRangeController.deleteCIDRBlock); // delete CIDR block
CIDRRangeRoutes.delete('/:CIDR_range_id', CIDRRangeController.deleteCIDRRange); // delete CIDR range (used for testing)

export default CIDRRangeRoutes;
