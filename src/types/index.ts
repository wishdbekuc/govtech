export interface ResponseHandler {
  status?: number;
  message?: string;
  data?: any;
  error?: any;
}
