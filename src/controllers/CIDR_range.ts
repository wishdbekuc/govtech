import { HttpStatusCode } from 'axios';
import { Handler, Request, Response } from 'express';
import { z } from 'zod';
import { prisma } from '..';
import { createResponse, getBlockSize, getCIDRSize, highestPowerOfTwo, parseCIDRString } from '../utils';

const getAllCIDRRanges: Handler = async (req: Request, res: Response) => {
  try {
    const allRanges = await prisma.cidr_range.findMany({
      select: {
        id: true,
        name: true,
        cidr: true,
      },
    });
    createResponse(res, {
      data: allRanges,
    });
  } catch (error: Error | any) {
    createResponse(res, {
      status: HttpStatusCode.InternalServerError,
      error: 'Something went wrong',
    });
  }
};

const getAllCIDRBlocks: Handler = async (req: Request, res: Response) => {
  const { CIDR_range_id } = getAllCIDRBlocksParser.parse(req.params);
  try {
    const CIDRRange = await prisma.cidr_range.findFirst({
      where: {
        id: CIDR_range_id,
      },
      include: {
        cidr_blocks: {
          orderBy: {
            sort_order: 'asc',
          },
        },
      },
    });

    createResponse(res, {
      data: CIDRRange?.cidr_blocks,
    });
  } catch (error: Error | any) {
    createResponse(res, {
      status: HttpStatusCode.InternalServerError,
      error: 'Something went wrong',
    });
  }
};

const createCIDRRange: Handler = async (req: Request, res: Response) => {
  try {
    const { id, name, cidr } = createCIDRRangeParser.parse(req.body);
    const rangeSize = getCIDRSize(cidr);
    try {
      const existingCIDRRange = await prisma.cidr_range.findFirst({
        where: {
          id,
        },
      });
      if (existingCIDRRange) {
        createResponse(res, {
          status: HttpStatusCode.Conflict,
          error: `CIDR Range of id: ${id} already exists`,
        });
        return;
      }
      const { network_prefix, size } = parseCIDRString(cidr);
      const newCIDRRange = await prisma.cidr_range.create({
        data: {
          id,
          name,
          cidr,
          total_ip_adresses: rangeSize,
          available_ip_adresses: rangeSize,
          network_prefix: network_prefix,
          size,
        },
      });
      createResponse(res, {
        status: HttpStatusCode.Created,
        data: `Created`,
      });
    } catch (error: Error | any) {
      createResponse(res, {
        status: HttpStatusCode.InternalServerError,
        error: 'Something went wrong',
      });
    }
  } catch (error: Error | any) {
    createResponse(res, {
      status: HttpStatusCode.BadRequest,
      error: 'Unable to parse inputs',
    });
  }
};

const createCIDRBlock: Handler = async (req: Request, res: Response) => {
  try {
    const { CIDR_range_id } = createCIDRBlockParamsParser.parse(req.params);
    const { id, size } = createCIDRBlockBodyParser.parse(req.body);
    const blockSize = getBlockSize(size);
    try {
      const CIDRRange = await prisma.cidr_range.findFirst({
        where: {
          id: CIDR_range_id,
        },
      });
      if (!CIDRRange) {
        createResponse(res, {
          status: HttpStatusCode.BadRequest,
          error: `CIDR Range of id: ${CIDR_range_id} was not found`,
        });
        return;
      }
      if (CIDRRange.available_ip_adresses < blockSize) {
        createResponse(res, {
          status: HttpStatusCode.BadRequest,
          error: `CIDR Range of id: ${CIDR_range_id} has insufficient available blocks`,
        });
        return;
      }
      await prisma.$transaction(async (tx) => {
        // find all unallocated blocks which are larger than or equal in size to new block
        // order these blocks by size (smallest first)
        const existingCIDRBlocks = await tx.cidr_block.findMany({
          where: {
            size: {
              lte: size,
            },
            allocated: false,
          },
          orderBy: {
            size: 'asc',
          },
        });
        if (existingCIDRBlocks.length > 0) {
          // create the new block at the first position in the existingCIDRBlocks array
          const reallocatedCIDRBlock = await tx.cidr_block.create({
            data: {
              id,
              cidr_range_id: existingCIDRBlocks[0].cidr_range_id,
              size,
              cidr: `${CIDRRange.network_prefix}/${size}`,
              allocated: true,
              sort_order: existingCIDRBlocks[0].sort_order,
            },
          });
          // delete the old block
          const oldCIDRBlock = await tx.cidr_block.delete({
            where: {
              id: existingCIDRBlocks[0].id,
            },
          });
          // split block if block that was deleted is larger than required
          if (existingCIDRBlocks[0].size !== size) {
            // number of additional blocks we need to split the original larger block into
            const numberOfBlocksToSplitInto = Math.pow(2, size - existingCIDRBlocks[0].size) - 1;
            const updateSortOrder = await tx.cidr_block.updateMany({
              where: {
                sort_order: {
                  gt: reallocatedCIDRBlock.sort_order,
                },
              },
              data: {
                sort_order: {
                  increment: numberOfBlocksToSplitInto,
                },
              },
            });
            // create the relevant number of new blocks
            Array.from({ length: numberOfBlocksToSplitInto }).forEach(async (_, index) => {
              const leftoverCIDRBlock = await tx.cidr_block.create({
                data: {
                  id: `${existingCIDRBlocks[0].id}_leftover_${index}`,
                  cidr_range_id: existingCIDRBlocks[0].cidr_range_id,
                  size,
                  cidr: `${CIDRRange.network_prefix}/${size}`,
                  allocated: false,
                  sort_order: existingCIDRBlocks[0].sort_order + index + 1,
                },
              });
            });
          }
        } else {
          // there are no unallocated blocks that can house this block
          // create a new block
          const latestBlock = await tx.cidr_block.findFirst({
            orderBy: {
              sort_order: 'desc',
            },
          });
          const newSortOrder = latestBlock ? latestBlock.sort_order + 1 : 0;
          const newCIDRBlock = await tx.cidr_block.create({
            data: {
              id,
              cidr_range_id: CIDR_range_id,
              size,
              cidr: `${CIDRRange.network_prefix}/${size}`,
              allocated: true,
              sort_order: newSortOrder,
            },
          });
        }

        // update the space left within the CIDR range
        const updateCIDRRange = await tx.cidr_range.update({
          where: {
            id: CIDR_range_id,
          },
          data: {
            available_ip_adresses: CIDRRange.available_ip_adresses - blockSize,
          },
        });
        createResponse(res, {
          status: HttpStatusCode.Created,
        });
      });
    } catch (error: Error | any) {
      createResponse(res, {
        status: HttpStatusCode.InternalServerError,
        error: 'Something went wrong',
      });
    }
  } catch (error: Error | any) {
    createResponse(res, {
      status: HttpStatusCode.BadRequest,
      error: 'Unable to parse inputs',
    });
  }
};

const deleteCIDRBlock: Handler = async (req: Request, res: Response) => {
  try {
    const { CIDR_range_id, CIDR_block_id } = deleteCIDRBlockParser.parse(req.params);
    try {
      const CIDRRange = await prisma.cidr_range.findFirst({
        where: {
          id: CIDR_range_id,
        },
      });
      if (!CIDRRange) {
        createResponse(res, {
          status: HttpStatusCode.BadRequest,
          error: `CIDR Range of id: ${CIDR_range_id} was not found`,
        });
        return;
      }
      const CIDRBlock = await prisma.cidr_block.findFirst({
        where: {
          id: CIDR_block_id,
        },
      });
      if (!CIDRBlock) {
        createResponse(res, {
          status: HttpStatusCode.BadRequest,
          error: `CIDR Block of id: ${CIDR_block_id} was not found`,
        });
        return;
      }
      const blockSize = getBlockSize(CIDRBlock.size);
      await prisma.$transaction(async (tx) => {
        // set allocated = false for block
        const unallocateCIDRBlock = await tx.cidr_block.update({
          where: {
            id: CIDR_block_id,
          },
          data: {
            allocated: false,
          },
        });
        // update available space in CIDR range
        const updateCIDRRange = await tx.cidr_range.update({
          where: {
            id: CIDR_range_id,
          },
          data: {
            available_ip_adresses: CIDRRange.available_ip_adresses + blockSize,
          },
        });

        // from the index of the unallocated block, travel up and down (based on sort order)
        // find the first and last index whereby the block is of the same size and is unallocated
        let startIndexToMerge = unallocateCIDRBlock.sort_order;
        let endIndexToMerge = unallocateCIDRBlock.sort_order;
        const allBlocks = await tx.cidr_block.findMany({
          orderBy: {
            sort_order: 'asc',
          },
        });
        const unallocatedBlockIndex = allBlocks.findIndex((block) => block.id === CIDR_block_id);
        const unallocatedBlockSize = unallocateCIDRBlock.size;
        let i = unallocatedBlockIndex;
        while (i >= 0 && allBlocks[i].size === unallocatedBlockSize && allBlocks[i].allocated === false) {
          startIndexToMerge = i--;
        }
        let j = unallocatedBlockIndex;
        while (j < allBlocks.length && allBlocks[j].size === unallocatedBlockSize && allBlocks[j].allocated === false) {
          endIndexToMerge = j++;
        }
        // we can only combine blocks if the qty of blocks is a power of 2
        // here we find the maximum num of blocks we can combine
        const maxNumToMerge = highestPowerOfTwo(endIndexToMerge - startIndexToMerge + 1);
        // delete all blocks we can combine
        for (let i = startIndexToMerge; i < startIndexToMerge + maxNumToMerge; i++) {
          const deleteBlock = await tx.cidr_block.delete({
            where: {
              id: allBlocks[i].id,
            },
          });
        }
        // set a new block at the starting index's sort order
        const mergedBlock = await tx.cidr_block.create({
          data: {
            id: `${allBlocks[startIndexToMerge].id}_merged`,
            cidr_range_id: allBlocks[startIndexToMerge].cidr_range_id,
            size: unallocatedBlockSize - Math.log2(maxNumToMerge),
            cidr: `${CIDRRange.network_prefix}/${unallocatedBlockSize + Math.log2(maxNumToMerge)}`,
            allocated: false,
            sort_order: allBlocks[startIndexToMerge].sort_order,
          },
        });
        // update sort orders for the rest of the blocks
        const updateSortOrder = await tx.cidr_block.updateMany({
          where: {
            sort_order: {
              gt: mergedBlock.sort_order,
            },
          },
          data: {
            sort_order: {
              decrement: maxNumToMerge - 1,
            },
          },
        });
        createResponse(res, {
          status: HttpStatusCode.NoContent,
          data: `Deleted`,
        });
      });
    } catch (error: Error | any) {
      createResponse(res, {
        status: HttpStatusCode.InternalServerError,
        error: 'Something went wrong',
      });
    }
  } catch (error: Error | any) {
    createResponse(res, {
      status: HttpStatusCode.BadRequest,
      error: 'Unable to parse inputs',
    });
  }
};

const deleteCIDRRange: Handler = async (req: Request, res: Response) => {
  try {
    const { CIDR_range_id } = deleteCIDRRangeParser.parse(req.params);
    try {
      const CIDRRange = await prisma.cidr_range.findFirst({
        where: {
          id: CIDR_range_id,
        },
      });
      if (!CIDRRange) {
        createResponse(res, {
          status: HttpStatusCode.BadRequest,
          error: `CIDR Range of id: ${CIDR_range_id} was not found`,
        });
        return;
      }
      const deleteCIDRRange = await prisma.cidr_range.delete({
        where: {
          id: CIDR_range_id,
        },
      });
      createResponse(res, {
        status: HttpStatusCode.NoContent,
        data: `Deleted`,
      });
    } catch (error: Error | any) {
      createResponse(res, {
        status: HttpStatusCode.InternalServerError,
        error: 'Something went wrong',
      });
    }
  } catch (error: Error | any) {
    createResponse(res, {
      status: HttpStatusCode.BadRequest,
      error: 'Unable to parse inputs',
    });
  }
};

const createCIDRRangeParser = z.object({
  id: z.string(),
  name: z.string(),
  cidr: z.string(),
});

const getAllCIDRBlocksParser = z.object({
  CIDR_range_id: z.string(),
});

const createCIDRBlockBodyParser = z.object({
  id: z.string(),
  size: z.coerce.number(),
});

const createCIDRBlockParamsParser = z.object({
  CIDR_range_id: z.string(),
});

const deleteCIDRBlockParser = z.object({
  CIDR_range_id: z.string(),
  CIDR_block_id: z.string(),
});

const deleteCIDRRangeParser = z.object({
  CIDR_range_id: z.string(),
});

export default {
  getAllCIDRRanges,
  createCIDRBlock,
  createCIDRRange,
  deleteCIDRBlock,
  deleteCIDRRange,
  getAllCIDRBlocks,
};
