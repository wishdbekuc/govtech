import { Response } from 'express';
import { ResponseHandler } from '../types';

// http status defaults to 200 if status is not defined
export const createResponse = (res: Response, responseParams: ResponseHandler): void => {
  const { status, data: responseData, error } = responseParams;
  res.status(status || 200).json({ data: responseData, error: error || null });
};

export const getCIDRSize = (cidrRange: string) => {
  const numOfBits = 32 - parseInt(cidrRange.split('/')[1]);

  if (numOfBits < 0) {
    throw new Error();
  }
  return Math.pow(2, numOfBits);
};

export const getBlockSize = (size: number) => {
  const numOfBits = 32 - size;

  if (numOfBits < 0) {
    throw new Error();
  }
  return Math.pow(2, numOfBits);
};

export const parseCIDRString = (cidrString: string) => {
  const cidrArr = cidrString.split('/');
  return {
    network_prefix: cidrArr[0],
    size: parseInt(cidrArr[1]),
  };
};

export const highestPowerOfTwo = (num: number) => {
  return Math.pow(2, Math.floor(Math.log2(num)));
};
