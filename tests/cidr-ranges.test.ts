import request from 'supertest';
import app from '../src'; // Adjust the path to your Express app
import { cleanup } from './cleanup';
import { HttpStatusCode } from 'axios';

beforeAll(async () => {
  await cleanup();
});

describe('CIDR Range API', () => {
  it('should create a CIDR range', async () => {
    const response = await request(app).post('/cidr-ranges').send({
      id: 'test',
      name: 'test',
      cidr: '172.16.0.0/28',
    });
    expect(response.status).toBe(HttpStatusCode.Created);
  });

  it('should get all CIDR ranges', async () => {
    const response = await request(app).get('/cidr-ranges');
    expect(response.status).toBe(HttpStatusCode.Ok);
    expect(response.body.data.filter((range: any) => range.id === 'test').length === 1).toEqual(true);
  });

  it('should create a CIDR block', async () => {
    const response = await request(app).post('/cidr-ranges/test/blocks').send({
      id: 'testBlock',
      size: 29,
    });
    expect(response.status).toBe(HttpStatusCode.Created);
  });

  it('should create a CIDR block', async () => {
    const response = await request(app).post('/cidr-ranges/test/blocks').send({
      id: 'testBlock2',
      size: 29,
    });
    expect(response.status).toBe(HttpStatusCode.Created);
  });

  // CIDR range is fully allocated, unable to create a new block
  it('should NOT create a CIDR block', async () => {
    const response = await request(app).post('/cidr-ranges/test/blocks').send({
      id: 'testBlock3',
      size: 29,
    });
    expect(response.status).toBe(HttpStatusCode.BadRequest);
  });

  it('should delete a CIDR block', async () => {
    const response = await request(app).delete('/cidr-ranges/test/blocks/testBlock');
    expect(response.status).toBe(HttpStatusCode.NoContent);
  });

  it('should delete a CIDR block', async () => {
    const response = await request(app).delete('/cidr-ranges/test/blocks/testBlock2');
    expect(response.status).toBe(HttpStatusCode.NoContent);
  });
});

afterAll(async () => {
  await cleanup();
});
