import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

// cleanup function to run before and after running tests
export const cleanup = async () => {
  // Delete all CIDR blocks
  await prisma.cidr_block.deleteMany({
    where: {
      id: {
        contains: 'test',
      },
    },
  });

  // Delete all CIDR ranges
  await prisma.cidr_range.deleteMany({
    where: {
      id: 'test',
    },
  });
};
